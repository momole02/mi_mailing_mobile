class StudentClass {
  String id;
  String label;

  StudentClass({this.id, this.label});

  factory StudentClass.fromJson(var json) {
    return StudentClass(
      id: json['id'],
      label: json['label'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "label": label,
    };
  }
}
