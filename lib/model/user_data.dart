class UserData {
  String id;
  String kind;
  String fullName;
  String email;
  String birthDate;
  String phoneNumber;

  UserData({
    this.id,
    this.kind,
    this.fullName,
    this.email,
    this.birthDate,
    this.phoneNumber,
  });

  factory UserData.fromJson(var json) {
    return UserData(
      id: json['id'],
      kind: json['kind'],
      fullName: json['full_name'],
      email: null != json['email'] ? json['email'] : null,
      birthDate: null != json['birth_date'] ? json['birth_date'] : null,
      phoneNumber: null != json['phone_number'] ? json['phone_number'] : null,
    );
  }

  Map<String, String> toJson() {
    return {
      'id': id,
      'kind': kind,
      'full_name': fullName,
      'email': email,
      'birth_date': birthDate,
      'phone_number': phoneNumber,
    };
  }
}
