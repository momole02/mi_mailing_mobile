class Student {
  String id;
  String fullName;
  String phoneNumber;
  String email;
  String classId;

  Student({
    this.id,
    this.fullName,
    this.phoneNumber,
    this.email,
    this.classId,
  });

  factory Student.fromJson(var json) {
    return Student(
      id: json['id'],
      fullName: json['full_name'],
      phoneNumber: json['phone_number'],
      email: json['email'],
      classId: json['class_id'],
    );
  }
}
