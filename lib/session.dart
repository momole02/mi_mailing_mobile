import 'dart:convert';

import 'package:mi_mailing_mobile/model/user_data.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Session {
  void loginUser(UserData data) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('user_data', json.encode(data.toJson()));
  }

  Future<UserData> loggedUser() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String data = preferences.getString('user_data');
    if (data == null) {
      return null;
    }
    return UserData.fromJson(json.decode(data));
  }

  Future<void> logout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.remove('user_data');
  }
}
