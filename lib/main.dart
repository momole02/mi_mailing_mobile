import 'package:flutter/material.dart';
import 'package:mi_mailing_mobile/pages/home.dart';
import 'package:mi_mailing_mobile/pages/login.dart';
import 'package:mi_mailing_mobile/pages/message_form.dart';
import 'package:mi_mailing_mobile/pages/recipients.dart';
import 'package:mi_mailing_mobile/pages/splash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MiMailing',
      theme: ThemeData(
        primaryColor: Color.fromARGB(255, 0, 128, 128),
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(),
    );
  }
}
