import 'package:flutter/material.dart';
import 'package:mi_mailing_mobile/commons/misc.dart';
import 'package:mi_mailing_mobile/model/student.dart';
import 'package:mi_mailing_mobile/model/student_class.dart';
import 'package:mi_mailing_mobile/requests/stud_class_list.dart';
import 'package:mi_mailing_mobile/requests/students_list.dart';

class RecipientsPage extends StatefulWidget {
  final List<Student> recipients;

  RecipientsPage({this.recipients});
  @override
  _RecipientsPageState createState() => _RecipientsPageState();
}

class _RecipientsPageState extends State<RecipientsPage> {
  bool ready = false;
  List<Student> students;
  List<Student> filteredStudents;
  List<StudentClass> studentClasses;
  Map<Student, int> selectedStudents = {};
  String searchFilter = '';

  @override
  void initState() {
    super.initState();
    loadAllData();
  }

  void loadAllData() async {
    setState(() => ready = false);
    students = await StudentListRequest().getStudentList();
    filteredStudents = students;
    studentClasses = await StudentClassListRequest().getStudentClasses();
    if (widget.recipients != null) {
      List<String> ids = widget.recipients.map((e) => e.id).toList();
      List<Student> recipients =
          students.where((element) => ids.contains(element.id)).toList();
      for (var stud in recipients) {
        selectedStudents[stud] = 1;
      }
    }
    setState(() => ready = true);
  }

  void onSearch(BuildContext context) {
    List<StudentClass> matchedClasses = studentClasses
        .where((element) => element.label.contains(searchFilter))
        .toList();
    List<String> matchedClassesIDs = matchedClasses.map((e) => e.id).toList();
    filteredStudents = students
        .where(
          (element) => ((element.fullName.contains(searchFilter)) ||
              (matchedClassesIDs.contains(element.classId))),
        )
        .toList();
  }

  void onSelectAll(BuildContext context) {
    for (var stud in filteredStudents) {
      selectedStudents[stud] = 1;
    }
    setState(() {});
  }

  void onUnselectAll(BuildContext context) {
    for (var stud in filteredStudents) {
      selectedStudents.remove(stud);
    }
    setState(() {});
  }

  void onValidate(BuildContext context) {
    List<Student> selectedStudentsList = selectedStudents.keys.toList();
    Navigator.of(context).pop(selectedStudentsList);
  }

  Widget buildListItem(BuildContext context, Student stud) {
    StudentClass studClass =
        studentClasses.firstWhere((element) => element.id == stud.classId);
    return Container(
      padding: EdgeInsets.all(3),
      child: InkWell(
        onTap: () {
          setState(() {
            if (selectedStudents.containsKey(stud)) {
              selectedStudents.remove(stud);
            } else {
              selectedStudents[stud] = 1;
            }
          });
        },
        child: Column(
          children: [
            Column(
              children: [
                Row(
                  children: [
                    !selectedStudents.containsKey(stud)
                        ? Icon(Icons.crop_square)
                        : Icon(Icons.check_box, color: Colors.green),
                    SizedBox(width: 16),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            stud.fullName,
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text("Classe : " + studClass.label),
                          SizedBox(height: 5),
                          Text(
                            "Tel : " + stud.phoneNumber,
                            style: TextStyle(fontSize: 12, color: Colors.grey),
                          ),
                          Text(
                            "Email : " + stud.email,
                            style: TextStyle(fontSize: 12, color: Colors.grey),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                Divider(
                  color: Colors.grey,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Destinaires"),
      ),
      body: Container(
        child: ready
            ? Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Icon(Icons.search),
                        SizedBox(width: 10),
                        Expanded(
                          child: TextFormField(
                            decoration: Misc(context).inputDecoration,
                            onChanged: (value) {
                              setState(() {
                                searchFilter = value;
                                onSearch(context);
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Divider(height: 5, color: Colors.grey),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      RaisedButton(
                        onPressed: () => onSelectAll(context),
                        child: Row(
                          children: [
                            Icon(Icons.check_box),
                            Text("Cocher tout"),
                          ],
                        ),
                      ),
                      SizedBox(width: 10),
                      RaisedButton(
                        onPressed: () => onUnselectAll(context),
                        child: Row(
                          children: [
                            Icon(Icons.crop_square),
                            Text("Décocher tout"),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Divider(height: 5, color: Colors.grey),
                  Expanded(
                    child: ListView.builder(
                      itemBuilder: (context, index) => buildListItem(
                        context,
                        filteredStudents[index],
                      ),
                      itemCount: filteredStudents.length,
                    ),
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: RaisedButton(
                          onPressed: () => onValidate(context),
                          color: Theme.of(context).primaryColor,
                          textColor: Colors.white,
                          child: Text("Valider"),
                        ),
                      ),
                    ],
                  )
                ],
              )
            : Center(
                child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(
                        Theme.of(context).primaryColor)),
              ),
      ),
    );
  }
}
