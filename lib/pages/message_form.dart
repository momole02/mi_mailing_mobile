import 'package:flutter/material.dart';
import 'package:mi_mailing_mobile/commons/misc.dart';
import 'package:mi_mailing_mobile/model/student.dart';
import 'package:mi_mailing_mobile/pages/recipients.dart';
import 'package:mi_mailing_mobile/requests/send_email.dart';
import 'package:mi_mailing_mobile/requests/send_sms.dart';
import 'package:mi_mailing_mobile/session.dart';

enum MessageType { SMS, Email }

class MessageFormPage extends StatefulWidget {
  final MessageType messageType;

  MessageFormPage({this.messageType});

  @override
  _MessageFormPageState createState() => _MessageFormPageState();
}

class _MessageFormPageState extends State<MessageFormPage> {
  TextEditingController controller;
  List<Student> recipients = [];

  String subject = '';
  String messageBody = '';
  String adminId = '';
  String status = '';

  bool sending = false;
  @override
  void initState() {
    super.initState();
    controller = new TextEditingController();
    sending = false;
    () async {
      adminId = (await Session().loggedUser()).id;
    }();
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  void onChooseRecipients(BuildContext context) async {
    List<Student> students = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => RecipientsPage(recipients: recipients)));
    if (null != students) {
      recipients = students;
      controller.value = TextEditingValue(
          text: students.map((e) => e.fullName).toList().join(" ; "));
    }
  }

  void onSend(BuildContext context) async {
    if (recipients.isEmpty || messageBody.isEmpty) {
      setState(() => status = "Remplissez le formulaire");
      return;
    }
    try {
      switch (widget.messageType) {
        case MessageType.Email:
          if (subject == null || subject.trim().isEmpty) {
            setState(() => status = "Remplissez le formulaire");
            return;
          }
          setState(() => sending = true);
          SendEmailRequest request = SendEmailRequest(
            studentsIDs: recipients.map((e) => int.tryParse(e.id)).toList(),
            subject: subject,
            body: messageBody,
            adminId: adminId,
          );
          int sentEmails = await request.sendEmails();
          status = "$sentEmails mails envoyé(s)";
          sending = false;
          break;
        case MessageType.SMS:
          setState(() => sending = true);
          SendSMSRequest request = SendSMSRequest(
            studentsIDs: recipients.map((e) => int.tryParse(e.id)).toList(),
            body: messageBody,
            adminId: adminId,
          );
          if (await request.sendSMS()) {
            status = "Requête de SMS envoyée";
            sending = false;
          }
          break;
      }
    } catch (e) {
      status = e.toString();
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Envoyer " +
            ((widget.messageType == MessageType.Email) ? "email" : "SMS")),
        actions: [
          PopupMenuButton<String>(
            itemBuilder: (context) {
              return [
                PopupMenuItem<String>(
                    child: Text("Ajouter aux msg. prédéfinis")),
                PopupMenuItem<String>(child: Text("Inserer msg. prédéfinis")),
              ];
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(status,
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 17,
                  )),
              (status != null && status.trim().isNotEmpty)
                  ? Divider(height: 15, color: Theme.of(context).primaryColor)
                  : SizedBox(),
              Text("Destinataires : "),
              SizedBox(height: 5),
              Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      decoration: Misc(context).inputDecoration,
                      controller: controller,
                      readOnly: true,
                      style: TextStyle(color: Colors.blue),
                    ),
                  ),
                  SizedBox(width: 5),
                  InkWell(
                    onTap: () => onChooseRecipients(context),
                    child: Container(
                      padding: EdgeInsets.all(13),
                      decoration: BoxDecoration(
                          color: Color.fromARGB(255, 0, 64, 128),
                          borderRadius: BorderRadius.circular(10)),
                      child: Icon(
                        Icons.edit,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20),
              widget.messageType == MessageType.Email
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Objet :"),
                        SizedBox(height: 5),
                        TextFormField(
                          onChanged: (value) => subject = value,
                          decoration: Misc(context).inputDecoration,
                        ),
                      ],
                    )
                  : SizedBox(),
              SizedBox(height: 20),
              Text("Message :"),
              SizedBox(height: 5),
              TextFormField(
                onChanged: (value) => messageBody = value,
                decoration: Misc(context).inputDecoration,
                keyboardType: TextInputType.multiline,
                maxLines: 6,
              ),
              SizedBox(height: 10),
              !sending
                  ? Row(
                      children: [
                        Expanded(
                          child: RaisedButton(
                            child: Text("ENVOYER"),
                            color: Theme.of(context).primaryColor,
                            textColor: Colors.white,
                            onPressed: () => onSend(context),
                          ),
                        ),
                      ],
                    )
                  : Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(
                            Theme.of(context).primaryColor),
                      ),
                    )
            ],
          ),
        ),
      ),
    );
  }
}
