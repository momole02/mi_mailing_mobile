import 'package:flutter/material.dart';
import 'package:mi_mailing_mobile/model/user_data.dart';
import 'package:mi_mailing_mobile/pages/login.dart';
import 'package:mi_mailing_mobile/pages/message_form.dart';
import 'package:mi_mailing_mobile/session.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Future<UserData> userDataFuture;
  @override
  void initState() {
    super.initState();
    loadUserData();
  }

  void loadUserData() {
    userDataFuture = Session().loggedUser();
  }

  void onLogout() async {
    await Session().logout();
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LoginPage()), (route) => false);
  }

  void onSendSMS(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => MessageFormPage(
          messageType: MessageType.SMS,
        ),
      ),
    );
  }

  void onSendEmail(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => MessageFormPage(
          messageType: MessageType.Email,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Accueil"),
        ),
        drawer: buildDrawer(context),
        body: FutureBuilder(
          future: userDataFuture,
          builder: (context, AsyncSnapshot<UserData> snapshot) {
            return snapshot.hasData
                ? SingleChildScrollView(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      child: Column(
                        children: [
                          Center(
                            child: Image.asset(
                              "assets/logo.png",
                              width: 128,
                            ),
                          ),
                          SizedBox(height: 24),
                          Text(
                            "Connecté en tant que : ${snapshot.data.fullName}",
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 10),
                          Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Theme.of(context).primaryColor,
                            ),
                            child: Text(
                              ((snapshot.data.kind == 'admin')
                                  ? "Sécrétaire géneral"
                                  : "Etudiant"),
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          SizedBox(height: 32),
                          snapshot.data.kind == 'admin'
                              ? Column(
                                  children: [
                                    Divider(
                                      height: 20,
                                      thickness: 1.5,
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        buildStatThumbnail(
                                          color:
                                              Color.fromARGB(255, 128, 0, 64),
                                          title: "999",
                                          legend: "Mails envoyés",
                                        ),
                                        buildStatThumbnail(
                                          color:
                                              Color.fromARGB(255, 0, 64, 128),
                                          title: "999",
                                          legend: "SMS envoyés",
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 20),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        buildStatThumbnail(
                                          color: Theme.of(context).primaryColor,
                                          title: "999",
                                          legend: "Etudiants",
                                        ),
                                        buildStatThumbnail(
                                          color:
                                              Color.fromARGB(255, 128, 64, 0),
                                          title: "999",
                                          legend: "Classes",
                                        ),
                                      ],
                                    ),
                                  ],
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                  )
                : SizedBox();
          },
        ));
  }

  Container buildStatThumbnail({
    @required Color color,
    @required String title,
    @required String legend,
  }) {
    return Container(
      padding: EdgeInsets.all(15),
      width: 150,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Column(
        children: [
          Text(
            title,
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
            ),
          ),
          Text(
            legend,
            style: TextStyle(
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }

  Drawer buildDrawer(BuildContext context) {
    return Drawer(
        child: FutureBuilder(
      future: userDataFuture,
      builder: (context, snapshot) => ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            child: Column(
              children: [
                FutureBuilder(
                  future: userDataFuture,
                  builder: (context, AsyncSnapshot<UserData> snapshot) {
                    return snapshot.hasData
                        ? Text(
                            "@ ${snapshot.data.fullName}",
                            style: TextStyle(color: Colors.white),
                          )
                        : SizedBox();
                  },
                ),
                SizedBox(height: 15),
                Container(
                  constraints: BoxConstraints(maxWidth: 140),
                  child: RaisedButton(
                    onPressed: () => onLogout(),
                    color: Colors.green[500],
                    textColor: Colors.white,
                    child: Row(
                      children: [
                        Icon(Icons.logout),
                        Text(
                          "  Se déconnecter",
                          style: TextStyle(fontSize: 10),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
            decoration: BoxDecoration(color: Theme.of(context).primaryColor),
          ),
          (snapshot.hasData && snapshot.data.kind == 'admin')
              ? Column(
                  children: [
                    ListTile(
                      title: Row(
                        children: [
                          Icon(Icons.sms),
                          SizedBox(width: 16),
                          Text("Envoyer SMS"),
                        ],
                      ),
                      onTap: () => onSendSMS(context),
                    ),
                    ListTile(
                      title: Row(
                        children: [
                          Icon(Icons.email),
                          SizedBox(width: 16),
                          Text("Envoyer Email"),
                        ],
                      ),
                      onTap: () => onSendEmail(context),
                    ),
                    ListTile(
                      title: Row(
                        children: [
                          Icon(Icons.dashboard_customize),
                          SizedBox(width: 16),
                          Text("Message prédéfinis"),
                        ],
                      ),
                      onTap: () {},
                    ),
                  ],
                )
              : SizedBox(),
          ListTile(
            title: Row(
              children: [
                Icon(Icons.person),
                SizedBox(width: 16),
                Text("Mon profil"),
              ],
            ),
            onTap: () {},
          ),
        ],
      ),
    ));
  }
}
