import 'package:flutter/material.dart';
import 'package:mi_mailing_mobile/commons/misc.dart';
import 'package:mi_mailing_mobile/pages/home.dart';
import 'package:mi_mailing_mobile/model/user_data.dart';
import 'package:mi_mailing_mobile/requests/login.dart';
import 'package:mi_mailing_mobile/session.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String login;
  String password;
  String errorMessage;
  bool waiting;

  @override
  void initState() {
    super.initState();
    login = '';
    password = '';
    errorMessage = '';
    waiting = false;
  }

  void onConnect(BuildContext context) async {
    setState(() => errorMessage = "");
    if (login.trim().isEmpty || password.trim().isEmpty) {
      setState(() => errorMessage = "Tous les champs sont obligatoires");
      return;
    }

    setState(() => waiting = true);
    try {
      UserData userData =
          await LoginRequest(login: login, password: password).doLogin();
      Session().loginUser(userData);
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => HomePage()),
        (route) => false,
      );
    } catch (e) {
      setState(() => errorMessage = e.toString());
    } finally {
      setState(() => waiting = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    var inputDecoration = Misc(context).inputDecoration;
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Image.asset(
                "assets/logo.png",
                width: 128,
              ),
            ),
            SizedBox(height: 32),
            Text(
              "Téléphone : ",
              style: TextStyle(color: Theme.of(context).primaryColor),
            ),
            SizedBox(height: 5),
            TextFormField(
              onChanged: (value) => login = value,
              decoration: inputDecoration,
            ),
            SizedBox(height: 32),
            Text(
              "Mot de passe",
              style: TextStyle(color: Theme.of(context).primaryColor),
            ),
            SizedBox(height: 5),
            TextFormField(
              onChanged: (value) => password = value,
              decoration: inputDecoration,
              obscureText: true,
            ),
            SizedBox(height: 10),
            Text(
              errorMessage,
              style: TextStyle(color: Colors.red),
            ),
            SizedBox(height: 25),
            Center(
              child: !waiting
                  ? RaisedButton(
                      onPressed: () => onConnect(context),
                      color: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      child: Text("CONNEXION"),
                    )
                  : CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Theme.of(context).primaryColor),
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
