import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mi_mailing_mobile/model/user_data.dart';
import 'package:mi_mailing_mobile/requests/base.dart';

class LoginRequest {
  String login;
  String password;

  LoginRequest({this.login, this.password});

  Future<UserData> doLogin() async {
    http.Response response = await http.post(
      HttpBase.ENDPOINT_URL + "/start.php?action=auth",
      body: json.encode(
        {
          "login": this.login,
          "password": this.password,
        },
      ),
    );
    int code = response.statusCode;

    debugPrint(
      response.request.url.toString() + " => " + response.body,
    );
    var responseJson = json.decode(response.body);
    if (code == 200) {
      return UserData.fromJson(responseJson['data']);
    } else {
      if (responseJson.containsKey('error')) {
        return Future.error(RequestError.fromJson(responseJson['error']));
      } else {
        throw new Exception(
            "Une erreur inattendue s'est produite, le serveur à retourné" +
                " ${response.statusCode} ${response.reasonPhrase} ");
      }
    }
  }
}
