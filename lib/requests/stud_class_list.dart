import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mi_mailing_mobile/model/student_class.dart';
import 'package:mi_mailing_mobile/requests/base.dart';

class StudentClassListRequest {
  Future<List<StudentClass>> getStudentClasses() async {
    http.Response response =
        await http.post(HttpBase.ENDPOINT_URL + "/start.php?action=classes");
    if (response.statusCode == 200) {
      var decoded = json.decode(response.body);
      var classesJson = decoded['classes'];
      List<StudentClass> classes = [];
      for (var classJson in classesJson) {
        classes.add(StudentClass.fromJson(classJson));
      }
      return classes;
    } else if (response.statusCode >= 400 && response.statusCode < 500) {
      var decoded = json.decode(response.body);
      return Future.error(RequestError.fromJson(decoded['error']));
    } else {
      throw new Exception("Erreur inattendue le serveur à retourné " +
          "${response.statusCode}/${response.reasonPhrase}");
    }
  }
}
