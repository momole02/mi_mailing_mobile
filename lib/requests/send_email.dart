import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mi_mailing_mobile/requests/base.dart';

class SendEmailRequest {
  List<int> studentsIDs;
  String subject;
  String body;
  String adminId;

  SendEmailRequest({
    this.studentsIDs,
    this.subject,
    this.body,
    this.adminId,
  });

  Future<int> sendEmails() async {
    http.Response response =
        await http.post(HttpBase.ENDPOINT_URL + "/start.php?action=sendmail",
            body: json.encode({
              "students": studentsIDs,
              "subject": subject,
              "body": body,
              "admin_id": adminId,
            }));
    if (response.statusCode == 200) {
      var decoded = json.decode(response.body);
      return decoded['sent_emails'];
    } else if (response.statusCode >= 400 && response.statusCode < 500) {
      var decoded = json.decode(response.body);
      return Future.error(decoded['error']);
    } else {
      throw new Exception(
          "Erreur inattendue : ${response.statusCode}/${response.reasonPhrase}");
    }
  }
}
