import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mi_mailing_mobile/requests/base.dart';

class SendSMSRequest {
  List<int> studentsIDs;
  String body;
  String adminId;

  SendSMSRequest({
    this.studentsIDs,
    this.body,
    this.adminId,
  });

  Future<bool> sendSMS() async {
    http.Response response =
        await http.post(HttpBase.ENDPOINT_URL + "/start.php?action=sendsms",
            body: json.encode({
              "students": studentsIDs,
              "body": body,
              "admin_id": adminId,
            }));
    if (response.statusCode == 200) {
      return true;
    } else if (response.statusCode >= 400 && response.statusCode < 500) {
      var decoded = json.decode(response.body);
      return Future.error(decoded['error']);
    } else {
      throw new Exception(
          "Erreur inattendue : ${response.statusCode}/${response.reasonPhrase}");
    }
  }
}
