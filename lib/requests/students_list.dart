import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mi_mailing_mobile/model/student.dart';
import 'package:mi_mailing_mobile/requests/base.dart';

class StudentListRequest {
  Future<List<Student>> getStudentList() async {
    http.Response response =
        await http.post(HttpBase.ENDPOINT_URL + "/start.php?action=students");

    if (response.statusCode == 200) {
      var decoded = json.decode(response.body);
      var studs = decoded['students'];
      List<Student> students = [];
      for (var studJson in studs) {
        students.add(Student.fromJson(studJson));
      }
      return students;
    } else if (response.statusCode >= 400 && response.statusCode < 500) {
      var decoded = json.decode(response.body);
      return Future.error(RequestError.fromJson(decoded['error']));
    } else {
      throw new Exception("Erreur inattenue : le serveur à retourné " +
          "${response.statusCode}/${response.reasonPhrase}");
    }
  }
}
