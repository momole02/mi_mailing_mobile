class HttpBase {
  static final String ENDPOINT_URL = "http://mimailing.loca.lt/api";
}

class RequestError {
  String code;
  String error;
  RequestError({this.code, this.error});
  factory RequestError.fromJson(var json) {
    return RequestError(code: json['code'], error: json['message']);
  }

  @override
  String toString() {
    return "$error. Code=$code";
  }
}
